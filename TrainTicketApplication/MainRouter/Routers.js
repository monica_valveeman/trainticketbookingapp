import React from 'react';
import {BrowserRouter,Route,Link, Router} from 'react-router-dom';
import EndPage from '../EndPage/EndPage';
import HomePage from '../Home/HomePage';
import LinkPage from '../Link To Booking/LinkPage';
import LoginPage from '../Login Module/LoginPage';
import AddPassengerOne from '../PassengersInformationForms/AdditionalPassengers/AddPassengerOne';
import AddPassengerTwo from '../PassengersInformationForms/AdditionalPassengers/AddPassengerTwo';
import ContactInformation from '../PassengersInformationForms/ContactInformation/ContactInformation';
import PaymentPage from '../PassengersInformationForms/Payment/PaymentPage';
import PersonalDetails from '../PassengersInformationForms/PersonalDetails';
import NotAvailableTrains from '../SearchStation/NotAvailableTrains/NotAvailableTrains';
import SearchStation from '../SearchStation/SearchStation';
import FirstAc from '../SearchStation/ViewTrainsOne/TrainDisplayOne/AvailableSeats/FirstAc';
import SecondarySeat from '../SearchStation/ViewTrainsOne/TrainDisplayOne/AvailableSeats/SecondarySeat';
import SleeperClass from '../SearchStation/ViewTrainsOne/TrainDisplayOne/AvailableSeats/SleeperClass';
import TrainDisplayOne from '../SearchStation/ViewTrainsOne/TrainDisplayOne/TrainDisplayOne';
import AC3TierOne from '../SearchStation/ViewTrainsOne/TrainDisplayTwo/AvailableSeats/AC3TierOne';
import SecondarySeatOne from '../SearchStation/ViewTrainsOne/TrainDisplayTwo/AvailableSeats/SecondarySeatOne';
import SleeperClassOne from '../SearchStation/ViewTrainsOne/TrainDisplayTwo/AvailableSeats/SleeperClassOne';
import TrainDisplayTwo from '../SearchStation/ViewTrainsOne/TrainDisplayTwo/TrainDisplayTwo';
import ViewTrainsOne from '../SearchStation/ViewTrainsOne/ViewTrainsOne';
import AC3TierThree from '../SearchStation/ViewTrainsTwo/TrainDisplayFour/AvailableSeats/AC3TierThree';
import SecondarySeatThree from '../SearchStation/ViewTrainsTwo/TrainDisplayFour/AvailableSeats/SecondarySeatThree';
import TrainDisplayFour from '../SearchStation/ViewTrainsTwo/TrainDisplayFour/TrainDisplayFour';
import AC2TierTwo from '../SearchStation/ViewTrainsTwo/TrainDisplayThree/AvailableSeats/AC2TierTwo';
import FirstAcTwo from '../SearchStation/ViewTrainsTwo/TrainDisplayThree/AvailableSeats/FirstAcTwo';
import SecondarySeatTwo from '../SearchStation/ViewTrainsTwo/TrainDisplayThree/AvailableSeats/SecondarySeatTwo';
import TrainDisplayThree from '../SearchStation/ViewTrainsTwo/TrainDisplayThree/TrainDisplayThree';
import ViewTrainsTwo from '../SearchStation/ViewTrainsTwo/ViewTrainsTwo';

function Routers(){
    return(
            <div>
                <BrowserRouter>
                    <Route exact path='/home' component={HomePage}/>
                    <Route path='/loginpage' component={LoginPage}/>
                    <Route path='/loginagain' component={LoginPage} />
                    <Route path='/linktobooking' component={LinkPage}/>

                    <Route path='/searchtrains' component={SearchStation}/>
                    <Route path='/notavailabletrains' component={NotAvailableTrains}/>
                    
                    <Route path='/viewtrains' component={ViewTrainsOne}/>
                    
                    <Route path='/traindisplayone' component={TrainDisplayOne}/>
                    <Route path='/secondaryseat' component={SecondarySeat}/>
                    <Route path="/firstac" component={FirstAc}/>
                    <Route path="/sleeperclass" component={SleeperClass}/>

                    <Route path='/traindisplaytwo' component={TrainDisplayTwo}/>
                    <Route path="/secondaryseatone" component={SecondarySeatOne}/>
                    <Route path="/ac3tier" component={AC3TierOne}/>
                    <Route path='/sleeperclassone' component={SleeperClassOne}/>

                    <Route path='/viewtrainstwo' component={ViewTrainsTwo}/>

                    <Route path='/traindisplaythree' component={TrainDisplayThree}/>
                    <Route path='/secondaryseattwo' component={SecondarySeatTwo}/>
                    <Route path='/firstactwo' component={FirstAcTwo}/>
                    <Route path='/ac2tiertwo' component={AC2TierTwo}/>

                    <Route path='/traindisplayfour' component={TrainDisplayFour}/>
                    <Route path='/secondaryseatthree' component={SecondarySeatThree}/>
                    <Route path='/ac3tierthree' component={AC3TierThree}/>

                    <Route path='/passengerdetails' component={PersonalDetails}/>
                    <Route path='/addpassenger1' component={AddPassengerOne}/>
                    <Route path='/addpassenger2' component={AddPassengerTwo}/>
                    <Route path='/contactdetails' component={ContactInformation}/>
                    <Route path='/paymentdetails' component={PaymentPage}/>

                    <Route path='/ending' component={EndPage}/>


                    </BrowserRouter>
                    </div>
    )
}
export default Routers;
