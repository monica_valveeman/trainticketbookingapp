import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import {createPassenger} from '../Redux/actionTypes';
import { FaUserCircle } from 'react-icons/fa';

class LoginPage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            isLogi:false,
            isicon:<FaUserCircle/>
        };
    }
    handle=({target}={})=>{
        if (target){
            if (target.name==='username')
            this.setState({username:target.value})
            else if (target.name='password')
            this.setState({password:target.value})
        }
    }
    han=()=>{
        this.setState({isLogi:true});
    }
    formSubmit=(e)=>{
        e.preventDefault();
        this.setState({isLogi:true});
        this.props.createNewPassenger(this.state);
        console.log(this.props.passengers);
        this.props.history.push("/linktobooking");
    }
    render(){
        let {username,password}=this.state;
        
        return(
            <center> 
                <br/><br/><br/>
                <div className="logincontainer">
                    <br/><br/><br/>
                    <h1>Login here</h1>
                    <br/><br/>
                        <form onSubmit={(e)=>this.formSubmit(e)}>
                            <input type="text" placeholder="UserName" name='username' value={username} onChange={(eve)=>this.handle(eve)}/><br/><br/>
                            <input type="password" placeholder="Password" name='password' value={password} onChange={(eve)=>this.handle(eve)}/><br/><br/>
                            <Button type="submit" onClick={this.han} variant="primary">Login</Button>
                        </form>
                 </div>
            </center>
        )
    }
}
function mapStatetoProps(state){
    return {
        passengers:state.passengers
    }
}
function mapDispatchtoProps(dispatch){
    return{
        createNewPassenger(passenger){
            dispatch(createPassenger(passenger));
        }
    }

}
export default connect(mapStatetoProps,mapDispatchtoProps)(LoginPage);