import React from 'react';
import axios from 'axios';
import Select from 'react-select';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import '../CSS Files/design2.css';

class SearchStation extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            selectedoption:'',
            selectedoption1:'',
            persons: []
        }  
    }
    componentDidMount() {
        axios.get(`https://run.mocky.io/v3/501e871f-c1f2-402e-bc64-e4899e60ca44`)
          .then(res => {
            console.log(res);
            this.setState({ persons : res.data});
        })
    }
    handlechange=(selectedoption)=> {
        this.setState({selectedoption});
        console.log(selectedoption.label);
        if (selectedoption.label==='Tambaram'){
            console.log('hi');
        }
    }
    handlechang1=(selectedoption1)=>{
        this.setState({selectedoption1});
        console.log(selectedoption1.label);
    }
    han=()=>{
         let selectedoption=this.state.selectedoption;
         let opt=selectedoption.label;
         let selectedoption1=this.state.selectedoption1;
         let opt1=selectedoption1.label
         console.log(opt);
         console.log(opt1);
         if (opt==='Chennai Egmore' && opt1==='Madurai Junction')
            this.props.history.push("/viewtrains");
        else if (opt==='Madurai Junction' && opt1==='Chennai Egmore')
            this.props.history.push("/viewtrainstwo");
        else
            this.props.history.push("/notavailabletrains");
    }
    render(){
        return(
            <div>
                <div className="search">
                <br/>
                    <center>
                        <form>
                            <table>
                                <tr>
                                    <td align="right"><label className="labelsel"><b>From:</b></label></td>
                                    <td align="left"> 
                                        <div className="wrapper">
                                            <Select className="Select"
                                                options={this.state.persons.map(per=>{return {label:per.name}})}  
                                                value={this.state.value}
                                                onChange={this.handlechange}>
                                            </Select>
                                        </div>
                                    </td>
                                    <td>
                                        <pre>         </pre>
                                    </td>
                                    <td align="right"> <label className="labelsel"><b>To:</b></label></td>
                                    <td align="left"> 
                                        <div className="wrapper">
                                            <Select className="Select"
                                                options={this.state.persons.map(per=>{return {label:per.name}})}
                                                value={this.state.value}
                                                onChange={this.handlechang1}> 
                                            </Select>
                                        </div>
                                    </td><pre>         </pre>
                                    <td> <Button type="submit" onClick={this.han} variant="danger">Search Trains</Button></td>
                                </tr>
                            </table>
                        </form>
                        <br/>
                    </center>
                </div>
            </div>
        )
    }
}
export default SearchStation;