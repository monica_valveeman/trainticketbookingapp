import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Card,Spinner,Button,Row} from 'react-bootstrap';

class AC2TierTwo extends React.Component{
    constructor(props){
        super(props);
        this.state={
            Train1:[],
            isLoading:true
        }
    }
    componentDidMount(){
        axios.get(`https://run.mocky.io/v3/914fde09-7191-4cd2-a66c-a267c4ea4609`)
        .then(res=>{
            console.log(res);
            this.setState({Train1: res.data,isLoading:false});
         } )
    }
    details=(e)=>{
        e.preventDefault();
        this.props.history.push('/passengerdetails');
    }
    goback=(e)=>{
        e.preventDefault();
        this.props.history.push('/traindisplaythree');
    }
    render(){
        return(
            <div>
                <br/><br/>
                <center>
                    {this.state.isLoading?<Spinner animation="border" />:
                        <Container className="rr">
                            <p>Train Name: Tn Ms Exp</p>
                            <p>Train No: 02694</p>
                            <p> <Button variant="primary" onClick={this.goback}>Goback</Button></p>       
                            <td>
                                {this.state.Train1.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.scf.ddate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.scf.dseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.scf.damt}
                                            </Card.Text>
                                         </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                                    
                                {this.state.Train1.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.scs.edate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.scs.eseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.scs.eamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                            
                                {this.state.Train1.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.sct.fdate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.sct.fseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.sct.famt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}
                            </td>
                        </Container> 
                    }
                </center>
            </div>
        )
    }
}
export default AC2TierTwo;