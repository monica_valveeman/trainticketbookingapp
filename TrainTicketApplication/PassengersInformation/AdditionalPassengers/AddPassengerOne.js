import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';

class AddPassengerOne extends React.Component{
    constructor(props){
        super(props);
        this.state={
            fname:'',fnamevalid:'',
            lname:'',lnamevalid:'',
            gender:'',gendervalid:'',
            age:'',agevalid:''
        };
    }
    firstname=(e)=>{
        e.preventDefault();
        this.setState({fname:e.target.value});
        if (e.target.value===''){
            this.setState({fnamevalid:'*required'});
        }
        else if (!(/^[a-z\s]+$/i.test(e.target.value))){
             this.setState({fnamevalid:'*only alphabets'});
        } 
        else {
            this.setState({fnamevalid:''});
        }  
    }
    lastname=(e)=>{
        e.preventDefault();
        this.setState({lname:e.target.value});
        if (e.target.value===''){
            this.setState({lnamevalid:'*required'});
        }
        else if (!(/^[a-z\s]+$/i.test(e.target.value))){
            this.setState({lnamevalid:'only alphabets'});
        } 
        else {
            this.setState({lnamevalid:''});
        } 
    }
    gender=(e)=>{
        e.preventDefault();
        this.setState({gender:e.target.value});
        if (e.target.value===''){
            this.setState({gendervalid:'Must select your gender'});
        } 
        else {
            this.setState({gendervalid:''});
        }
    }
    age=(e)=>{
        e.preventDefault();
        this.setState({age:e.target.value});
        if(e.target.value===''){
            this.setState({agevalid:'*required'});
        }
        else if(!(/^\d+$/).test(e.target.value)){
            this.setState({agevalid:'*invalid age'});
        }
        else{
            this.setState({agevalid:''});
        }
    }
      
    display=(e)=>{   
        e.preventDefault();
        this.props.history.push('/contactdetails');
    }
    add=(e)=>{
        e.preventDefault();
        this.props.history.push('/addpassenger2');
    }
    render(){
        let {fname,fnamevalid,lname,lnamevalid,gender,gendervalid,age,agevalid}=this.state;
        return(
            <div>
                <br/><br/><br/>
                <div>
                    <center>
                        <form>
                            <div className="travellerinformation">
                                <h3 className="hh">Additional Traveller 1 Information</h3>
                                <br/>
                                <center>
                                    <table>
                                        <tr>
                                            <td align="right"><label className="ff">First Name: </label></td>
                                            <td align="left"><input type="text" className="fname" placeholder="First Name"  value={fname} onChange={this.firstname}/></td>
                                        </tr>
                   
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{fnamevalid}</p></td>
                                        </tr>
                  
                                        <tr>
                                            <td align="right"><label className="ff">LastName:</label></td>
                                            <td align="left"> <input type="text" className="lname" placeholder="Last Name" value={lname} onChange={this.lastname}/></td>
                                        </tr>
                   
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{lnamevalid}</p></td>
                                        </tr>
                    
                                        <tr>
                                            <td align="right"><label className="ff">Gender:</label></td>
                                            <td align="left">  
                                                <select value={gender} className="gender" onChange={this.gender}>
                                                    <option value="">Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Transgender">Transgender</option>
                                                </select>
                                            </td>
                                        </tr>
                    
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{gendervalid}</p></td>
                                        </tr>
                
                                        <tr>
                                            <td align="right"> <label className="ff">Age:</label></td>
                                            <td align="left"><input type="number" placeholder="Age" className="age" value={age} onChange={this.age}/></td>
                                        </tr>
                    
                                        <tr>
                                            <td></td>
                                            <td> <p className="pp">{agevalid}</p></td>
                                        </tr>
                                    </table>
                                </center>
                                <br/>
                                <Button type="submit" variant="light" onClick={this.add}>+ Add Passenger</Button>{'  '}
                                <Button type="submit" className="kk" onClick={this.display} variant="success">Continue Booking</Button>
                            </div>
                        </form>
                    </center>
                </div>
            </div>
        );
    }
}
export default AddPassengerOne;