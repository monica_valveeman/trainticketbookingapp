
import React from 'react';
import {connect} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Spinner} from 'react-bootstrap';
import Routers from '../MainRouters/Routers';
import '../CSS Files/design.css';

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isload:true
        }
    }
    componentDidMount(){
        this.setState({isload:false});
    }
    render(){
        let temp=this.props.passenger.username;
        console.log(temp);
        let logo=this.props.passenger.isLogi;
        console.log(logo);
        let iconic=this.props.passenger.isicon;
        console.log(iconic);
        let pay;
        let holo=this.props.passenger.apicall;
        console.log(holo);
        const apply=()=>{
             if (this.props.passenger.array===''){
                console.log("No ");
            }
            else {
            // pay=this.props.passenger.apicall.map((each)=><p>{each.atname}</p>);
            }
        }
        const disp=()=>{
            if(logo===true)
                return <Button variant="light"><a href="/loginagain">Logout</a></Button>;
            else 
                return <Button variant="light"> <a href='/loginpage'>Login</a></Button>
        }
        let title=this.props.title;
        if(!(temp instanceof Object))
        {
            title=temp;
            console.log(this.props.passenger.username);
        }
        return(
            <header className="head">
                {this.state.isload?<Spinner animation="border" />:<h1>{apply()}</h1>}
                <h1 className="head1">Train Ticket Booking App</h1>
                <h5 className='pass'><b> {title}</b>{iconic}</h5>
                <p className="links"><Button variant="light"><a href='/home'>Home</a></Button>{'  '}{disp()}</p>
                <p><Routers/></p>
                <div>
                </div>
            </header>
            
        );
    }
}
function mapStatetoProps(state){
    return{
        passenger:state,
    }
}
export default connect (mapStatetoProps)(Header);